var baseUrl = "";
var baseApi = baseUrl + "api/";
var baseLogin = baseApi + "login";
var baseAdmin = baseUrl + "admin/";
var baseSession = baseApi + "session/";
var baseStageApi = baseApi + "stage/";
var baseView = baseUrl + "View/";
var baseImage = baseUrl + "upload/image/";
var baseResourceImage = baseUrl + "res/image/";
var basePageNotFound = baseUrl + "pageNotFound";
var baseSetupApi = baseApi + "setup/";
var basePlay = baseUrl + "play/";
var baseUploadImage = baseUrl + "upload/image/"

var func = {};
var validityRadio = {};

var prod = false;;

//add base tag
if (window.location.href.includes("localhost")) {
    document.write("<base href='http://" + document.location.host + "/induction/' />");
} else {
    document.write("<base href='http://" + document.location.host + "/' />");
}

$.getScript('res/js/toastr.min.js', function () {
    $(document).ready(function () {

        //check prod
        checkProd();

        if (window.location.href.includes("admin") || true) {
            setTimeout(function () {
                document.getElementById("initialLoader").classList.add("remove");
                setTimeout(function () {
                    removeElement(document.getElementById("initialLoader"));
                }, 100);
            }, 1000);
        }

        if (window.location.href.includes("admin")) checkSession();
    });
});

function checkProd() {// environment changes based on url

    if (!window.location.href.includes("localhost")){
        prod = true;
        baseUrl = window.location.origin + "/";
    }else{
        baseUrl = window.location.origin + "/induction/";
    }
    if (prod) {
        window.console.log = function () { };
    }
}

function view(url, elementID = "content", base = baseAdminView, append = false, history = false, param = null, callback) {

    new Http(base + url + ".html", function (success) {
        if (history) window.history.pushState({}, "", window.location.href);
        if (append) {
            document.getElementById(elementID).innerHTML += success;
        } else {
            document.getElementById(elementID).innerHTML = success;
        }
        if (typeof func[url] == "function") func[url](param);
        if (typeof callback == "function") callback(param);
    }).post({}, false, true, true, 1);
}

function loadMains(footer = "content", callback) {
    view("sidebar", "sidebar", baseAdminView, false, false, null, function () {
        view("footer", footer, baseAdminView, true, false, null, callback);
    });

}

function confirm(title = "Rent2Tour", message, success, error, buttons = ["Confirm", "Cancel"]) {
    $('#confirm').modal('show');
    $('#confirm')[0].style.zIndex = "10001";

    //send to foreground
    var modals = document.getElementsByClassName("modal-backdrop fade show");
    modals[modals.length - 1].style.zIndex = "10000";

    $('#confirmAccept')[0].innerHTML = buttons[0];
    if (buttons.length > 1) {
        $('#confirmClose')[0].innerHTML = buttons[1];
        $('#confirmClose').removeClass("d-none");
    } else {
        $('#confirmClose').addClass("d-none");
    }
    $('#confirmBody')[0].innerHTML = message;
    $('#confirmTitle')[0].innerHTML = title;

    $('#confirmClose').unbind().click(function () {
        if (typeof error == "function") error();
    });
    $('#confirmAccept').unbind().click(function () {
        if (typeof success == "function") success();
    });
}



function curday(sp, offset = null) {
    today = moment();
    var dd = (offset == null ? today.toDate().getDate() : today.add(offset['day'], "days").toDate().getDate());
    var mm = (offset == null ? today.toDate().getMonth() + 1 : today.add(offset['month'], "months").toDate().getMonth() + 1);
    var yyyy = (offset == null ? today.toDate().getFullYear() : today.add(offset['year'], "years").toDate().getFullYear());

    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    return (dd + sp + mm + sp + yyyy);
};

function validate(parent = null) {
    if (parent == null) parent = document.body;
    var elems = $(parent).find("input, textarea");
    var ret = true;
    var password = "";
    clearValidateError();

    for (var i = 0; i < elems.length; i++) {
        var elem = elems[i];
        var validity;
        if (elem.getAttribute("required") != null && (!(validity = isInputValid(elem)).result || (elem.value == "" && elem.innerHTML == ""))) {
            if (elem.getAttribute("password-repeat") != null && password != (elem.value || elem.innerHTML)) {
                validity['msg'] = "Passwords do not match";
            }
            ret = false;
            $(parent).animate({ scrollTop: $(elem).offset().top }, 100);
            var error = elem.getAttribute("error") || validity.msg;
            displayElementError(elem, error);
        }

        if (elem.getAttribute("password") != null) {
            password = elem.value || elem.innerHTML;
        }
    }

    // for(var key in validityRadio){
    //     if(!validityRadio[key]){
    //         displayElementError(document.getElementsByClassName[key], "Please Choose One")
    //     }
    // }

    return ret;
}

function displayElementError(elem, error) {
    var id = generate(15);
    var newNode = createElement("div", { class: "validateError position-relative", id: id }, [], { innerHTML: error });
    insertAfter(newNode, elem);
    elem.style.marginBottom = "1em";
    elem.style.border = "1px solid red!important";
}

function result(parent = document.body) {
    if (validate(parent)) {
        var result = {};
        var chosen = [];
        var collections = $(parent).find(".collection");
        var groups = $(parent).find(".group");

        if (collections.length > 0) {
            for (var collection of collections) {
                var innerResult = [];
                $(collection).find(".group").each((k, elem) => {
                    var inner2 = {}
                    $(elem).find("input,textarea").each((j, innerElem) => {
                        inner2[innerElem.getAttribute("result")] = innerElem.value || innerElem.innerHTML;
                        chosen.push(innerElem);
                    });
                    innerResult.push(inner2);
                });
                result[collection.getAttribute("result")] = innerResult;
            }
        } else if (groups.length > 0) {
            for (var group of groups) {
                var name = group.getAttribute('result');
                result[name] = {};
                $(group).find("input,textarea").each((i, elem) => {
                    result[name][elem.getAttribute('result')] = elem.innerHTML || elem.value;
                    chosen.push(elem);
                })
            }
        }

        $(parent).find("input,textarea,select").each((i, elem) => {
            //console.log(i,elem);
            if (elem.getAttribute("result") != null && chosen.indexOf(elem) == -1) {
                var type = elem.type;
                //console.log(type);
                if (type == "number" || type == "text" || type == "phone" || type == "email" || type == "textarea" || type == "password" || type == "date") {
                    result[elem.getAttribute("result")] = elem.value || elem.innerHTML;
                } else if (type == "radio") {
                    if (elem.checked) result[elem.getAttribute("result")] = elem.getAttribute("value");
                } else if (type == "select-one") {
                    result[elem.getAttribute("result")] = elem.options[elem.selectedIndex].value;
                }
            }

        });
        return result;
    } else {
        return false;
    }
}


function checkSession(callback) {
    if (localStorage.getItem("userToken") == null) {
        if (!window.location.href.includes("login")) redirectToLogin();
    } else {
        new Http(baseSessionTest, function () {
            if (typeof callback == "function") callback()
            if (window.location.href.includes("login")) loginSuccessful();
        }, function () {
            redirectToLogin();
        }).post({}, false, false);
    }

}

function isInputValid(input) {
    var type = input.type;
    var value = input.innerHTML || input.value;
    var ret = true;
    var msg = "This Field is Required";
    //console.log(type);
    switch (type) {
        case "email":
            if (!value.includes("@") || !value.includes(".")) {
                ret = false
                msg = "Invalid Email Format"
            }
            break;
        case "number":
            if (isNaN(parseInt(value))) {
                ret = false;
                msg = "Invalid Number";
            }
            break;
        case "phone":
            if (isNaN(parseInt(value)) || value > 30 || value < 4) {
                ret = false;
                msg = "Invalid Phone Number | Do not add brackets";
            }
            break;
        case "radio":
            if (typeof validityRadio[input.name]) {
                validityRadio[input.name] = false;
            }
            if (input.checked) validityRadio[input.name] = true;
    }

    if (input.getAttribute("min-length") != null && value.length < input.getAttribute("min-length")) {
        ret = false;
        msg += " Value is too Short (> " + input.getAttribute("min-length") + ")";
    }
    if (input.getAttribute("max-length") != null && value.length > input.getAttribute("max-length")) {
        ret = false;
        msg += " Value is too Long (< " + input.getAttribute("max-length") + ")";
    }
    return { result: ret, msg: msg };
}

function clearValidateError() {
    var errorElems = document.getElementsByClassName("validateError");
    while (errorElems.length > 0) {
        var errorElem = errorElems[0];
        errorElem.previousSibling.style.marginBottom = "auto";
        removeElement(errorElem);
    }
}

function removeElement(elem, animate = false) {
    //console.log(elem, animate);
    if (!animate) {
        elem.parentNode.removeChild(elem);
    } else {
        $(elem).animate({ height: '0' }, 1000, "swing", function () {
            $(this).animate({ width: '0' }, 1000, "swing", function () {
                //console.log(this);
                removeElement(this);
            }.bind(elem));
        }.bind(elem));
    }
}

function removeIndirectElement(id, animate = false) {
    var elem = document.getElementById(id);
    //console.log("removeIndirectElement", id, elem, animate);
    removeElement(elem, animate);
}

function createElement(type, attributes, children = [], properties = {}) {
    var elem = document.createElement(type);
    for (var attribute in attributes) {
        elem.setAttribute(attribute, attributes[attribute]);
    }

    for (var i = 0; i < children.length; i++) {
        elem.appendChild(children[i]);
    }

    for (var property in properties) {
        if (property == "innerHTML") {
            elem[property] += properties[property];
        } else {
            elem[property] = properties[property];
        }
    }

    return elem;
}

function generate(length = 5) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function pascalCase(str) {
    if (typeof str == "string") {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    } else {
        return "";
    }

}


function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function Http(url, success, error) {
    this.url = url;
    this.success = success;
    this.error = error;
    this.count = 0;
    function post(params, loader = false, async = true, raw = false, reload = 3, rawParams = false, url = this.url, success = this.success, error = this.error, count = this.count) {

        if (window.navigator.onLine || true) {

            if (count < reload) {
                $.ajax({
                    type: "post",
                    url: url,
                    data: (rawParams ? params : { data: params }),
                    dataType: "json",
                    timeout: 10000,
                    beforesend: function () {
                        if (loader) displayLoader();
                    },
                    async: async,
                    headers: { "USER_TOKEN": localStorage.getItem("userToken") },
                    success: function (data) {
                        count = 0;
                        if (raw && typeof success == "function") {
                            success(data);
                        } else {
                            if (data['success']) {
                                if (typeof success == "function") success(data['data']);
                            } else {
                                if (data['error'] == "Session Expired") {
                                    redirectToLogin();
                                } else {
                                    toastr.error(data['error']);
                                }
                                if (typeof error == "function") error(data['error']);
                            }
                        }
                    },
                    error: function (data) {
                        if (raw && typeof success == "function") {
                            success(data.responseText || data);
                        } else {
                            if (typeof error == "function") error(data);
                            toastr.error("There has been an error");
                            count++;
                            post(params, loader, async, raw, reload, url, success, error, count);
                        }
                    }
                });
            } else {
                toastr.error("Oops! It looks like an error occured on my side. I will try to correct it ASAP");
            }

        } else {
            if (confirm("You Seem to be offline. Please Check your Connection and Try Again")) {
                post(params, loader, async, raw, reload, url, success, error, count);
            }
        }

    }

    this.post = post;
}

function get(url, options, success, error) {

}

function redirectToLogin() {
    var locations = window.location.href.split("/");
    var redirectURL = baseLoginPage + "/";
    for (var i = locations.indexOf("admin") + 1; i < locations.length; i++) {
        redirectURL += locations[i] + (i == locations.length - 1 ? "" : "/");
    }
    if (!window.location.href.includes("login")) window.location.href = redirectURL;
}
// Called when Google Javascript API Javascript is loaded
function HandleGoogleApiLibrary() {
    // Load "client" & "auth2" libraries
    gapi.load('client:auth2', {
        callback: function () {
            // Initialize client & auth libraries
            gapi.client.init({
                apiKey: 'AIzaSyDmmjcEM2ZvJp9FXLVDltcj3GFtH0erwV8',
                clientId: '37398873166-ajthefl2o9vjumn2nnqj1bsfn5t3h8s6.apps.googleusercontent.com',
                scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
            }).then(
                function (success) {
                    // Libraries are initialized successfully
                    // You can now make API calls
                },
                function (error) {
                    // Error occurred
                    // console.log(error) to find the reason
                }
            );
        },
        onerror: function () {
            // Failed to load libraries
        }
    });
}

function logout() {
    localStorage.removeItem("userToken");
    window.location.reload();
}

function loginSuccessful() {
    var locations = window.location.href.split("/");
    var loginIndex = locations.indexOf("login") || locations.indexOf("Login");
    if (loginIndex > -1) {
        var redirectURL = baseAdmin;
        for (var i = loginIndex + 1; i < locations.length; i++) {
            redirectURL += locations[i] + (i == locations.length - 1 ? "" : "/");
        }
        window.location.href = redirectURL;
    }

}


function displayUITable(elem, arr, conversion = {},
    inwardHooks = {},
    inwardOptions = { clear: true, image: true, dataTable: true },
    inwardHeadException = [], inwardBodyException = ["null"]) {

    //set inner variables
    hooks = {
        beforeBody: inwardHooks.beforeBody || null,
        afterBody: inwardHooks.afterBody || null,
        beforeHead: inwardHooks.beforeHead || null,
        afterHead: inwardHooks.afterHead || null,
        afterRow: inwardHooks.afterRow || null
    };
    var options = [];
    options['clear'] = typeof inwardOptions['clear'] == "undefined" ? true : inwardOptions['clear'];
    options['image'] = typeof inwardOptions['image'] == "undefined" ? true : inwardOptions['image'];
    options['dataTable'] = typeof inwardOptions['dataTable'] == "undefined" ? true : inwardOptions['dataTable'];
    var headException = ["vehicleID", "requestID"];
    for (var i = 0; i < inwardHeadException.length; i++) {
        headException.push(inwardHeadException[i]);
    }
    var bodyException = [];
    for (var i = 0; i < inwardBodyException.length; i++) {
        bodyException.push(inwardBodyException[i]);
    }

    if (options.clear) elem.innerHTML = "";
    if (arr.length <= 0) {
        elem.appendChild(createElement("h3", { class: "text-center p-3" }, [], { innerHTML: "No Data" }));
        return -1;
    }
    var id = generate();
    var table =
        createElement("table", { class: "table table-responsive table-hover w-100", id: "table-" + id }, [
            createElement("thead", { id: "tableHead-" + id, class: "thead-light" }),
            createElement("tbody", { id: "tableBody-" + id })
        ]);
    elem.appendChild(table);


    for (var i = 0; i < arr.length; i++) {
        var head = createElement("tr");
        var body = createElement("tr");
        for (var key in arr[i]) {
            var datum = arr[i][key];
            if (headException.indexOf(key) != -1 || (bodyException.indexOf(datum) != -1)) { } else {
                if (datum == null && !(options.image && key == "image")) datum = "None";
                var beforeBodyFunction;
                if (typeof hooks.beforeBody == "function") {
                    beforeBodyFunction = hooks.beforeBody(body, i, key, arr);
                    if (typeof beforeBodyFunction != "undefined" && beforeBodyFunction.node != false) {
                        var node;
                        if (!beforeBodyFunction.raw) {
                            node = createElement("td", {}, [beforeBodyFunction.node]);
                            body.appendChild(node);
                        } else {
                            nodes = beforeBodyFunction.node;
                            for (var node of nodes) {
                                body.appendChild(node);
                            }
                        }
                    };
                }

                if (typeof beforeBodyFunction == "undefined" || !beforeBodyFunction.stopProcessing) {
                    if (options.image && key == "image") {
                        body.appendChild(createElement("td", {}, [
                            createElement("img", { src: (datum != null ? baseImage + datum + "_small" : baseResourceImage + "noImage"), style: "max-height: 10vh;" })
                        ]));
                    } else {
                        body.appendChild(createElement("td", {}, [], { innerHTML: datum }));
                    }
                }


                if (i == 0) {
                    var beforeHeadFunction;
                    if (typeof hooks.beforeHead == "function") {
                        beforeHeadFunction = hooks.beforeHead(head, key, arr);
                        if (typeof beforeHeadFunction != "undefined" && beforeHeadFunction.node != false) {
                            var node;
                            if (!beforeHeadFunction.raw) {
                                node = createElement("th", {}, [beforeHeadFunction.node])
                            } else {
                                node = beforeHeadFunction.node;
                            }
                            head.appendChild(node);
                        };
                    }
                    if (typeof beforeHeadFunction == "undefined" || !beforeHeadFunction.stopProcessing) {
                        head.appendChild(createElement("th", {}, [], { innerHTML: (key in conversion ? conversion[key] : pascalCase(key)) }));
                    }

                }
            }
        }

        if (i == 0) {
            if (typeof hooks.afterHead == "function") head.appendChild(hooks.afterHead(head, i, arr));
            document.getElementById("tableHead-" + id).appendChild(head);
        }

        if (typeof hooks.afterBody == "function") body.appendChild(createElement("td", {}, [hooks.afterBody(body, i, arr)]));
        document.getElementById("tableBody-" + id).appendChild(body);
        if (typeof hooks.afterRow == "function") document.getElementById("tableBody-" + id).appendChild(hooks.afterRow(i, arr));

    }

    if (options.dataTable) $("#table-" + id).dataTable();

    return id;
}

function uploadFileCheck(elem) {

}

function formatDate(date, separator = "/", calendar = false) {
    if (typeof date == "String") date = parseFloat(date);
    var newDate = new Date(date * 1000);
    newDate.setMilliseconds(0);
    return (calendar ?
        newDate.getFullYear() + separator + newDate.getDate() + separator + newDate.getMonth() :
        newDate.getDate() + separator + newDate.getMonth() + separator + newDate.getFullYear());
}

function showCover(elem) {
    var cover = document.getElementsByClassName("cover")[0];
    cover.classList.add("active");
    elem.classList.add("active");
    cover.onclick = function () {
        elem.classList.remove('active');
        cover.classList.remove('active');
    }

}

function initImageZoom(){
    $(".imageZoom").click(function(e){
        window.open(e.target.src);
    });
}