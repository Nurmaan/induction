

function uploadImage(imageURI, send, onprogress, success, error) {
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/png";

    options.params = { data: send };
    options.chunkedMode = false;

    showProgressLoader();

    var ft = new FileTransfer();
    ft.onprogress = function (progressEvent) {
        if (progressEvent.lengthComputable) {
            //console.log(progressEvent.loaded / progressEvent.total);
            document.getElementById("progressLoaderValue").innerHTML = Math.round((progressEvent.loaded / progressEvent.total) * 100) + "%";
            if (typeof onprogress == "function") onprogress(progressEvent.loaded / progressEvent.total, progressEvent);
        }
    };
    ft.upload(imageURI, baseURL + "food.php", function (result) {
        hideProgressLoader();
        toastr.success("Image Uploaded!");
        if (typeof success == "function") success(result);
        console.log(result.responseText);
    }, function (err) {

        hideProgressLoader();
        toastr.error("Sorry an error occured while uploading. Check your internet connection and try again");
        if (typeof error == "function") error(err);
        console.log('error : ', err);
    }, options);
}