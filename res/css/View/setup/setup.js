$(document).ready(function(){
    getTeams();
    listenToInput();
    
});

function setup(){
    var res = result();
    if(res){
        new Http(baseSetupApi, function(data){
            toastr.success("Setup Completed");
            localStorage.setItem("userToken", data);
            setTimeout(function(){
                window.location.href = basePlay;
            }, 3000);
        }).post({requestType:"setup", data:res});
    }
}

function getRemainingAmount(){
    var team = result(document.getElementById("teamContainer"))['team'];
    if(team){
        new Http(baseSetupApi, function(data){
            document.getElementById("seats").innerHTML = data;
        }).post({requestType:"getRemainingAmount", team: team});
    }
}

function getTeams(){
    new Http(baseSetupApi, function(data){
        for(var datum of data){
            document.getElementById("team").appendChild(
                createElement("option", {value:datum}, [], {innerHTML: pascalCase(datum)})
            );
        }

        setInterval(getRemainingAmount, 1000);
    }).post({requestType: "getTeams"});
}
function listenToInput() {
    var input = document.getElementById("password");

    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            setup();
        }
    });
}
