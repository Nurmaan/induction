function previewEditCarImage(element, i = 0) {
    var image = document.getElementById("imageContainer");
    var files = document.getElementById(element).files;
    if (i == 0) image.innerHTML = "";
    if (i < files.length) {
        var reader = new FileReader();

        reader.onload = function (e) {
            image.appendChild(createElement("img", { class: "editCarImage float-left m-2", src: e.target.result }));
            i++;
            previewEditCarImage(element, i);
        }

        reader.readAsDataURL(files[i]);
    }
}

function save() {
    var res = {};
    if ((res = result())) {
        var files = document.getElementById("images").files;
        var fd = new FormData();
        for (var file of files) {
            fd.append("files[]", file);
        }
        fd.append('data', JSON.stringify(res));
        fd.append('requestType', 'addStage');
        $.ajax({
            url: baseStageApi,
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                window.location.reload();
            }
        });
    }
}