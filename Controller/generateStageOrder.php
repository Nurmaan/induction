<?php

$GLOBALS['success'] = true;
$GLOBALS['error'] = "A server error occured";
$GLOBALS['msg'] = "";
$GLOBALS['data'] = [];

//clear the stageorder table

$query = $GLOBALS['con']->prepare("DELETE FROM `stageorder`;");
$query->execute();

// get all teams
$query = $GLOBALS['con']->prepare("SELECT id FROM `team`;");
$teams = fetchAssoc($query, function($row){return $row['id'];});

// get all stages
$query = $GLOBALS['con']->prepare("SELECT id FROM `stage`;");
$stages = fetchAssoc($query, function($row){return $row['id'];});

//loop through all teams
$innerQuery = $GLOBALS['con']->prepare("INSERT INTO `stageorder` (teamID, stageID, password) VALUES (:teamID, :stageID, :password)");

foreach($teams as $team){
    shuffle($stages);
    $innerQuery->bindParam(":teamID", $team);
    foreach($stages as $stage){
        $password = generate(5);
        $innerQuery->bindParam(":stageID", $stage);
        $innerQuery->bindParam(":password", $password);
        $innerQuery->execute();
    }
}

header("Content-Type: application/json");

$ret = [];
$ret['success'] = $GLOBALS['success'];
$ret['data'] = $GLOBALS['data'];
$ret['error'] = $GLOBALS['error'];
$ret['msg'] = $GLOBALS['msg'];
echo json_encode($ret);
