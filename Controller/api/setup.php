<?php

$dat = $_POST['data'];
$GLOBALS['success'] = true;
$GLOBALS['error'] = "A server error occured";
$GLOBALS['msg'] = "";
$GLOBALS['data'] = [];

if (isset($dat['requestType'])) {
    call_user_func($dat['requestType']);
} else {
    call_user_func($_POST['requestType']);
}

header("Content-Type: application/json");

$ret = [];
$ret['success'] = $GLOBALS['success'];
$ret['data'] = $GLOBALS['data'];
$ret['error'] = $GLOBALS['error'];
$ret['msg'] = $GLOBALS['msg'];
echo json_encode($ret);

function setup()
{
    $data = $_POST['data']['data'];
    if (getRemainingAmount(true, $data['team']) > 0) { // check if there are spaces left in the team
        if (array_search(strtolower($data['team']), getTeams(true)) !== false) { // check if team name is correct
            if (checkTeamPassword($data['team'], $data['password'])) {
                $userToken = generate(100);
                $time = time();
                $team = getTeamID($data['team']);
                $query = $GLOBALS['con']->prepare("INSERT INTO `user` (team, time, userToken, username, lastOnline) VALUES (:team, :time, :userToken, :username, :lastOnline);");
                $query->bindParam(":team", $team);
                $query->bindParam(":time", $time, PDO::PARAM_INT);
                $query->bindParam(":userToken", $userToken);
                $query->bindParam(":username", $data['username']);
                $query->bindParam(":lastOnline", $time, PDO::PARAM_INT);
                $query->execute();
                $GLOBALS['data'] = $userToken;
            }else{
                $GLOBALS['success'] = false;
                $GLOBALS['error'] = "Invalid Password";
            }
        } else {
            $GLOBALS['success'] = false;
            $GLOBALS['error'] = "Invalid Team Name";
        }
    } else {
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "This Team is Already Filled Up";
    }
}

function checkTeamPassword($team, $password){
    $query = $GLOBALS['con']->prepare("SELECT name FROM team WHERE name=:name AND password=:password");
    $query->bindParam(":name", $team);
    $query->bindParam(":password", $password);
    $query->execute();
    return $query->rowCount() > 0;
}

function getRemainingAmount($ret = false, $team = "")
{ // get the available remaining users to login for that team

    return 20;

    if ($team == "") $team = $_POST['data']['team'];
    $team = getTeamID($team);
    $query = $GLOBALS['con']->prepare("SELECT time FROM `user` WHERE team = :team AND valid = 1;");
    $query->bindParam(":team", $team);
    $query->execute();
    if (!$ret) {
        $GLOBALS['data'] = 20 - $query->rowCount();
    } else {
        return 20 - $query->rowCount();
    }
}

function getTeams($ret = false)
{ // get all the team names and their teamID

    $query = $GLOBALS['con']->prepare("SELECT name, teamID FROM `team`;");
    $data = fetchAssoc($query, function($row){
            return $row['name'];
        });
    if (!$ret) {
        $GLOBALS['data'] = $data;
    } else {
        return $data;
    }
}

function getTeamID($name)
{
    $name = strtolower($name);
    $query = $GLOBALS['con']->prepare("SELECT id FROM `team` WHERE name=:name");
    $query->bindParam(":name", $name);
    $query->execute();
    return $query->fetch(PDO::FETCH_ASSOC)['id'];
}
