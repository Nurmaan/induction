<?php

function resizeImage($file, $newFileName, $maxWidth, $maxHeight){
    $fileName = $file["name"]; // The file name
    $fileTmpLoc = $file["tmp_name"]; // File in the PHP tmp folder
    $fileErrorMsg = $file["error"]; // 0 for false... and 1 for true
    $kaboom = explode(".", $fileName); // Split file name into an array using the dot
    $fileExt = end($kaboom); // Now target the last array element to get the file extension
    // START PHP Image Upload Error Handling --------------------------------------------------
    if (!$fileTmpLoc) { // if file not chosen
        return "ERROR: Please browse for a file before clicking the upload button.";
    } else if (!preg_match("/.(gif|jpg|png|jpeg)$/i", $fileName) ) {
        // This condition is only if you wish to allow uploading of specific file types    
        unlink($fileTmpLoc); // Remove the uploaded file from the PHP temp folder
        return "ERROR: Your image was not .gif, .jpg, or .png.";
        
        exit();
    } else if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        return "ERROR: An error occured while processing the file. Try again.";
    }
    // END PHP Image Upload Error Handling ----------------------------------------------------

    // ---------- Include Universal Image Resizing Function --------
    $target_file = $fileTmpLoc;
    $resized_file = "./upload/image/$newFileName";
    ak_img_resize($target_file, $resized_file, $maxWidth, $maxHeight, $fileExt);
    // ----------- End Universal Image Resizing Function -----------
}


function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){ 
      $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
      $img = imagecreatefrompng($target);
    } else { 
      $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 80);
}
?>