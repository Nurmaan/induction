<?php

$dat = $_POST['data'];
$GLOBALS['success'] = true;
$GLOBALS['error'] = "A server error occured";
$GLOBALS['msg'] = "";
$GLOBALS['data'] = [];
if (isset($dat['requestType'])) {
    call_user_func($dat['requestType']);
} else {
    call_user_func($_POST['requestType']);
}

function addStage()
{
    $dat = json_decode($_POST['data'], true);
    $stageID = generate(100);
    $query = $GLOBALS['con']->prepare("INSERT INTO `stage` (hint, location, stageID) VALUES (:hint, :location, :stageID);");
    $query->bindParam(":hint", $dat['hint']);
    $query->bindParam(":location", $dat['location']);
    $query->bindParam(":stageID", $stageID);

    if ($query->execute() && isset($_FILES["files"])) {
        $files = $_FILES["files"]["name"];
        include_once('./Controller/api/resizeImage.php');
        for ($i = 0; $i < sizeof($files); $i++) {
            $exp = explode(".", $_FILES["files"]["name"][$i]);
            $exp = end($exp);
            $newName = mt_rand();
            $newNameRegular = $newName;
            $newNameSmall = $newName . "_small";
            $file = [];
            $file["name"] = $_FILES["files"]["name"][$i];
            $file["tmp_name"] = $_FILES["files"]["tmp_name"][$i];
            $file["type"] = $_FILES["files"]["type"][$i];
            $file["size"] = $_FILES["files"]["size"][$i];
            $file["error"] = $_FILES["files"]["error"][$i];
            resizeImage($file, $newNameSmall, 400, 400);
            resizeImage($file, $newNameRegular, 1280, 800);
            $query = $GLOBALS['con']->prepare("INSERT INTO `image` (tbl, tblID, name) VALUES ('stage', (SELECT id FROM `stage` WHERE stageID = :stageID), '$newName');");
            $query->bindParam(":stageID", $stageID);
            $query->execute();
        }
    } else {
        $GLOBALS['success'] = false;
    }
}

function getStage($stage = -1)
{
    $data = $_POST['data'];
    if (isset($data['stage']) || $stage > -1) { // find a specific stage

    } else { // find all stages for this user
        $userToken = $data['userToken'];

        //get current stage progress and team id
        $query = $GLOBALS['con']->prepare("SELECT `team`.stage, `team`.id FROM `user` 
        INNER JOIN `team` ON `user`.team = `team`.id
        WHERE `user`.userToken = :userToken;
        ");
        $query->bindParam(":userToken", $userToken);
        $query->execute();
        $details = $query->fetch(PDO::FETCH_ASSOC);

        //get all stages for that team
        $query = $GLOBALS['con']->prepare("SELECT stageID FROM `stageorder` WHERE teamID=:teamID;");
        $query->bindParam(":teamID", $details['id']);
        $stages = fetchAssoc($query, function ($row) {
            return $row['stageID'];
        });
        $size = ($details['stage'] < sizeof($stages) ? $details['stage'] : sizeof($stages));
        for ($i = 0; $i < $size; $i++) {
            //get hint
            $innerQuery = $GLOBALS['con']->prepare("SELECT hint FROM `stage` WHERE id=:id;");
            $innerQuery->bindParam(":id", $stages[$i], PDO::PARAM_INT);
            $innerQuery->execute();
            $hint = $innerQuery->fetch(PDO::FETCH_ASSOC)['hint'];

            //get images
            $innerQuery = $GLOBALS['con']->prepare("SELECT name FROM `image` WHERE tblID=:id AND tbl='stage';");
            $innerQuery->bindParam(":id", $stages[$i], PDO::PARAM_INT);
            $images = fetchAssoc($innerQuery, function ($row) {
                return $row['name'];
            });

            //get stageID
            $innerQuery = $GLOBALS['con']->prepare("SELECT stageID FROM `stage` WHERE id=:id;");
            $innerQuery->bindParam(":id", $stages[$i], PDO::PARAM_INT);
            $innerQuery->execute();
            $stageID = $innerQuery->fetch(PDO::FETCH_ASSOC)['stageID'];

            $inner = [];

            //get location and equation if not the last stage
            if ($i < $size - 1 || $details['stage'] == 10) {
                //get location
                $innerQuery = $GLOBALS['con']->prepare("SELECT location FROM `stage` WHERE id=:id;");
                $innerQuery->bindParam(":id", $stages[$i], PDO::PARAM_INT);
                $innerQuery->execute();
                $location = $innerQuery->fetch(PDO::FETCH_ASSOC)['location'];

                //get equation
                $innerQuery = $GLOBALS['con']->prepare("SELECT name FROM `equation` WHERE id=:id;");
                $id = $i + 1;
                $innerQuery->bindParam(":id", $id, PDO::PARAM_INT);
                $innerQuery->execute();
                $equation = $innerQuery->fetch(PDO::FETCH_ASSOC)['name'];
                //output
                $inner['location'] = $location;
                $inner['equation'] = $equation;
            }

            //output details
            $inner['hint'] = $hint;
            $inner['images'] = $images;
            $inner['stageID'] = $stageID;
            $GLOBALS['data'][] = $inner;
        }
    }
}

function getStages($ret = false)
{ // get all the stage names and their stageID

    $query = $GLOBALS['con']->prepare("SELECT location, stageID FROM `stage`;");
    $data = fetchAssoc($query);
    if (!$ret) {
        $GLOBALS['data'] = $data;
    } else {
        return $data;
    }
}

function validate()
{
    $password = $_POST['data']['password'];
    $userToken = $_POST['data']['userToken'];
    $data = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT teamID FROM `stageorder` WHERE teamID=(SELECT team FROM `user` WHERE userToken=:userToken) AND password=:password AND stageID=(SELECT id FROM `stage` WHERE stageID=:stageID);");
    $query->bindParam(":userToken", $userToken);
    $query->bindParam(":password", $password);
    $query->bindParam(":stageID", $data['stageID']);
    $query->execute();
    if ($query->rowCount() == 0) {
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "Invalid Code";
    } else {
        //get location details
        $query = $GLOBALS['con']->prepare("SELECT location FROM `stage` WHERE stageID=:stageID;");
        $query->bindParam(":stageID", $data['stageID']);
        $query->execute();
        $GLOBALS['data'] = $query->fetch(PDO::FETCH_ASSOC)['location'];

        //get equation

        //update the team table if not already at last stage
        $time = time();
        $query = $GLOBALS['con']->prepare("UPDATE `team` SET stage = stage + 1, time = :time, 
        user = (SELECT username FROM `user` WHERE userToken=:userToken) 
        WHERE id = (SELECT team FROM `user` WHERE userToken=:userToken1) AND stage < 10;");
        $query->bindParam(":userToken", $data['userToken']);
        $query->bindParam(":userToken1", $data['userToken']);
        $query->bindParam(":time", $time);
        $query->execute();
    }
}

function check()
{
    $datum = [];
    $datum['state'] = false;
    $GLOBALS['data'][] = $datum;
    // $GLOBALS['data'][] = checkUser();
    // $GLOBALS['data'][] = checkValidUser();
    // $GLOBALS['data'][] = checkCompleted();
}

function checkCompleted(){
    $data = $_POST['data'];
    $query=$GLOBALS['con'] -> prepare("SELECT completed FROM `team` WHERE id=(SELECT team FROM `user` WHERE userToken=:userToken) AND NOT completed='no'");
    $query->bindParam(":userToken", $data['userToken']);
    $query->execute();
    $res = [];
    if ($query->rowCount() > 0) {
        $res['state'] = true;
        $res['user'] = $query->fetch(PDO::FETCH_ASSOC)['completed'];
    } else {
        $res['state'] = false;
    }
    $res['type'] = "completed";
    return $res;
}
function checkStage()
{
    $data = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT `team`.user FROM `team` WHERE id = (SELECT team FROM `user` WHERE userToken=:userToken) AND time > :time;");
    $query->bindParam(":userToken", $data['userToken']);
    $query->bindParam(":time", $data['time'], PDO::PARAM_INT);
    $query->execute();
    $res = [];
    if ($query->rowCount() > 0) {
        $res['state'] = true;
        $res['user'] = $query->fetch(PDO::FETCH_ASSOC)['user'];
    } else {
        $res['state'] = false;
    }
    $res['type'] = "stage";
    return $res;
}

function checkValidUser()
{
    $res = [];
    if (checkSession()) {
        updateSession();
        $res['type'] = "removeUser";
        $data = $_POST['data'];
        $query = $GLOBALS['con']->prepare("SELECT time, lastOnline, invalidSince, valid, username, id FROM `user`;");
        $query->bindParam(":userToken", $data['userToken']);
        $ret = fetchAssoc($query, function ($row, $post) {
            $data = $_POST['data'];
            if ($row['valid'] == 0) {
                if ($row['invalidSince'] > $data['time']) {
                    return $row['username'];
                }
            } else {
                if ($row['lastOnline'] < time() - 15 * 3600) {
                    $time = time();
                    $query = $GLOBALS['con']->prepare("UPDATE user SET valid=0, invalidSince=:time WHERE id=:id");
                    $query->bindParam(":id", $row['id'], PDO::PARAM_INT);
                    $query->bindParam(":time", $time, PDO::PARAM_INT);
                    $query->execute();

                    return $row['username'];
                }
            }
        });

        if(sizeof($res) > 0){
            $res['state'] = true;
            $res['user'] = $ret;
        }else{
            $res['state'] = false;
        }
    } else {
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "You Have Been Logged Out Due To Innactivity";
    }

    return $res;
}

function checkSession()
{
    $data = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT lastOnline FROM `user` WHERE userToken = :userToken;");
    $query->bindParam(":userToken", $data['userToken']);
    $query->execute();
    $lastOnline = $query->fetch(PDO::FETCH_ASSOC)['lastOnline'];
    if ($lastOnline > (time() - 36000)) {
        return true;
    } else {
        $time = time();
        $query = $GLOBALS['con']->prepare("UPDATE user SET valid=0 AND invalidSince=:time WHERE userToken=:userToken");
        $query->bindParam(":userToken", $data['userToken']);
        $query->bindParam(":time", $time, PDO::PARAM_INT);
        $query->execute();
        return false;
    }
}

function updateSession()
{
    $data = $_POST['data'];
    $time = time();
    $query = $GLOBALS['con']->prepare("UPDATE `user` SET lastOnline=:time WHERE userToken=:userToken");
    $query->bindParam(":userToken", $data['userToken']);
    $query->bindParam(":time", $time, PDO::PARAM_INT);
    $query->execute();
}

function checkUser()
{
    $data = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT username FROM `user` WHERE time > :time;");
    $query->bindParam(":time", $data['time'], PDO::PARAM_INT);
    $query->execute();
    $res = [];
    if ($query->rowCount() > 0) {
        $res['state'] = true;
        $res['user'] = fetchAssoc($query, function ($row) {
            return $row['username'];
        });
    } else {
        $res['state'] = false;
    }
    $res['type'] = "user";
    return $res;
}

function getPasswords(){
    $data = $_POST['data'];
    if($data['password'] == "Sup3r4dm1n"){
        $query = $GLOBALS['con']->prepare("SELECT `team`.name, `stageorder`.password FROM `stageorder`
        INNER JOIN `team` ON `team`.id = `stageorder`.teamID
        WHERE `stageorder`.stageID = (SELECT id FROM `stage` WHERE stageID=:stageID);");
        $query->bindParam(":stageID", $data['stageID']);
        $GLOBALS['data'] = fetchAssoc($query);
    }else{
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "Invalid Password";
    }
}

function checkCode(){
    $data = $_POST['data'];
    $time = time();
    if($data['password'] == 6801){
        //update team table
        $query = $GLOBALS['con']->prepare("UPDATE `team` SET completed=(SELECT username FROM `user` WHERE userToken=:userToken),
        completeTime = :time
        WHERE `team`.id = (SELECT team FROM `user` WHERE userToken=:userToken);");
        $query->bindParam(":userToken", $data['userToken']);
        $query->bindParam(":userToken", $data['userToken']);
        $query->bindParam(":time", $time);
        $query->execute();

        //get info to send email
        $query = $GLOBALS['con']->prepare("SELECT completed, name, FROM_UNIXTIME(completeTime) as completeTime FROM `team` WHERE `team`.id = (SELECT team FROM `user` WHERE userToken=:userToken);");
        $query->bindParam(":userToken", $data['userToken']);
        $query->execute();
        $ret = $query->fetch(PDO::FETCH_ASSOC);
        mail("rnurmaanr@gmail.com", "[Middlesex] Hunt Completed", "The hunt has been completed by team $ret[name] and user $ret[completed] at $ret[completeTime];");
    }else{
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "Incorrect Code 😥";
    }
}

function getCompleted(){
    $data = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT name, stage, completed, FROM_UNIXTIME(completeTime) as completedTime FROM `team`;");
    $GLOBALS['data'] = fetchAssoc($query);
}

header("Content-Type: application/json");

$ret = [];
$ret['success'] = $GLOBALS['success'];
$ret['data'] = $GLOBALS['data'];
$ret['error'] = $GLOBALS['error'];
$ret['msg'] = $GLOBALS['msg'];
echo json_encode($ret);
