<?php

$dat = $_POST['data'];
$GLOBALS['success'] = true;
$GLOBALS['error'] = "A server error occured";
$GLOBALS['msg'] = "";
$GLOBALS['data'] = [];
if (isset($dat['requestType'])) {
    call_user_func($dat['requestType']);
} else {
    call_user_func($_POST['requestType']);
}

function sessionTest()
{
    $dat = $_POST['data'];
    $query = $GLOBALS['con']->prepare("SELECT time FROM `user` WHERE userToken=:userToken AND valid=1");
    $query->bindParam(":userToken", $dat['userToken']);
    $query->execute();
    if ($query->rowCount() == 0) {
        $GLOBALS['success'] = false;
        $GLOBALS['error'] = "Invalid Session";
    }
}

header("Content-Type: application/json");

$ret = [];
$ret['success'] = $GLOBALS['success'];
$ret['data'] = $GLOBALS['data'];
$ret['error'] = $GLOBALS['error'];
$ret['msg'] = $GLOBALS['msg'];
echo json_encode($ret);
