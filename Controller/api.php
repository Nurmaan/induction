<?php

$target = $GLOBALS['target'];
//userValid();
if (file_exists("Controller/api/$target[1].php")) {
    include_once("Controller/api/$target[1].php");
} else {
    view("pageNotFound");
}


function userValid()
{
    if($GLOBALS['target'][1] == "login") return;
    if ($GLOBALS['userToken']) {
        $userToken = $GLOBALS['userToken'];
        if ($userToken != null && $userToken != "null") {
            $query = $GLOBALS['con']->prepare("SELECT userToken FROM `user` WHERE userToken = :userToken AND valid = 1 AND blocked = 0;");
            $query->bindParam(":userToken", $userToken);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                output("Account Inactive Verify Your Account or <a href='mailto:contact@rent2tour.com'>Contact Us</a>");
            }
        }else{
            $query = $GLOBALS['con']->prepare("SELECT userToken FROM `user` WHERE email = :email AND valid = 1 AND blocked = 0;");
            $query->bindParam(":email", $_POST['data']['email']);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                output("Account Inactive Verify Your Account or <a href='mailto:contact@rent2tour.com'>Contact Us</a>");
            }
        }
    } else {
        output("User Token Absent");
    }
}

function isTokenValid()
{
    return true;
    if ($GLOBALS['userToken']) {
        $userToken = $GLOBALS['userToken'];
        $query = $GLOBALS['con']->prepare("SELECT id, UNIX_TIMESTAMP(time) as time FROM `token` WHERE userToken = :userToken AND valid = 1 ORDER BY time DESC LIMIT 1;");
        $query->bindParam(":userToken", $userToken);
        $query->execute();
        $res =  $query->fetch(PDO::FETCH_ASSOC);

        if (sizeof($res) > 0) {
            if (time() - ($res['time']) > 900) {
                $query = $GLOBALS['con']->prepare("UPDATE `token` SET valid = 0 WHERE id = :id;");
                $query->bindParam(":id", $res['id'], PDO::PARAM_INT);
                $query->execute();
                output("Session Expired");
            } else {
                $query = $GLOBALS['con']->prepare("UPDATE `token` SET time = now() WHERE id = :id;");
                $query->bindParam(":id", $res['id'], PDO::PARAM_INT);
                $query->execute();
                return true;
            }
        } else {
            output("Session Expired");
        }
    } else {
        output("User Token Absent");
    }
}


