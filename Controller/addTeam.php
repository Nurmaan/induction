<?php
return;
$teams = ["black river", "flacq", "grand port", "moka", "pamplemousses", "plaines-wilhems", "port-louis", "riviere du rempart", "savanne", "rodrigues"];
$query = $GLOBALS['con']->prepare("INSERT INTO `team` (teamID, name, password) VALUES(:teamID, :name, :password);");
foreach($teams as $team){
    $teamID = generate(100);
    $password = generate(5);
    echo "Inserting $team with id $teamID</br>";
    $query->bindParam(":teamID", $teamID);
    $query->bindParam(":name", $team);
    $query->bindParam(":password", $password);
    $query->execute();
}