<?php
function controller($name)
{
	if (file_exists("Controller/$name.php")) {
		include_once("Controller/$name.php");
	} else {
		view($name);
	}
}

function view($name, $raw = false)
{
	if ($raw) {

		if (file_exists($name)) {
			echo (file_get_contents($name));
		} else {
			view("pageNotFound");
		}
	} else {

		if (file_exists("View/$name/$name.html")) {
			echo (file_get_contents("View/$name/$name.html"));
		} else {
			view("pageNotFound");
		}
	}
}

function model($function, $name = "")
{

	try {
		if (name != "") include_once("Model/$name.php");
		call_user_func($function);
	} catch (Exception $e) {
		print("Could not load the model $name");
	}
}

function initDatabase()
{
	$servername = "";
	$username = "";
	$password = "";
	$dbName = "";
	switch (strtolower($GLOBALS['environment'])) {
		case "development":
			$servername = "localhost";
			$username = "root";
			$password = "";
			$dbName = "induction";
			break;
		case "testing":
			$servername = "localhost";
			$username = "nurmaan_rent";
			$password = "UK}67BiXzXW^";
			$dbName = "nurmaan_induction";
			break;
		case "production":
			$servername = "localhost";
			$username = "nurmaan_rent";
			$password = "UK}67BiXzXW^";
			$dbName = "nurmaan_induction";
			break;
	}


	$GLOBALS['con'] = new PDO("mysql:host=$servername;dbname=$dbName", "$username", "$password");
}

initDatabase();

/*------------------- helper -------------------*/
function output($error = "An error has occured", $fatal = true, $success = false, $data = [],  $msg = "")
{
    header("Content-Type: application/json");
    $ret = [];
    $ret['success'] = $success;
    $ret['data'] = $data;
    $ret['error'] = $error;
    $ret['msg'] = $msg;
    echo json_encode($ret);
    if ($fatal) exit(0);
}

function generate($len)
{
    $seed = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    shuffle($seed);
    $rand = '';
    for ($i = 0; $i < $len; $i++) {
        $rand .= $seed[array_rand($seed, 1)];
    }
    return $rand;
}

function fetchAssoc(PDOStatement $query, $callback = null, $one = false, $field = "")
{
    $query->execute();
    $ret = [];
    while ($res = $query->fetch(PDO::FETCH_ASSOC)) {
        if (!$one) {
            if ($callback != null) {
                $callbackValue = $callback($res, $_POST);
                if ($callbackValue != false) {
                    $ret[] = $callbackValue;
                } else {
                    continue;
                }
            } else {
                $ret[] = $res;
            }
        } else {
            $ret[] = $res[$field];
        }
    }
    return $ret;
}
