<?php

$GLOBALS['environment'] = "testing"; // development/testing/production

error_reporting(E_ALL);
ini_set('display_errors', TRUE);

$target = explode("/", $_SERVER['REQUEST_URI']);


switch (strtolower($GLOBALS['environment'])) {
    case "development":
        array_shift($target);
        array_shift($target);
        $GLOBALS['userToken'] = (isset(apache_request_headers()["USER_TOKEN"]) ? apache_request_headers()["USER_TOKEN"] : null);
        break;
    case "testing":
        array_shift($target);
        $GLOBALS['userToken'] = (isset(apache_request_headers()["User-Token"]) ? apache_request_headers()["User-Token"] : null);
        break;
    case "production":
        $GLOBALS['userToken'] = (isset(apache_request_headers()["User-Token"]) ? apache_request_headers()["User-Token"] : null);
        break;
}
$GLOBALS["target"] = $target;

include_once("Controller/comfunc.php");
if ($target[0] == "") {
    view('setup');
} else {
    controller($target[0]);
}
