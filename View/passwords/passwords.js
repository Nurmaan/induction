var stageData = [];
var stageLength = 10;
var currentStage = -1;
var lastCheckedTime = 0;

$(document).ready(function () {
    getTeams();
});

function getPasswords() {
    var res = result();
    if (res) {
        new Http(baseStageApi, function (data) {
            displayUITable(document.getElementById("result"), data);
            $(".table")[0].classList.remove("table-responsive");
            $(".table")[0].classList.add("w-100");
        }).post({ requestType: "getPasswords", password: res['password'], stageID: res['stage'] });

    }

}

function getTeams() {
    new Http(baseStageApi, function (data) {
        for (var datum of data) {
            document.getElementById("stage").appendChild(
                createElement("option", { value: datum['stageID'] }, [], { innerHTML: pascalCase(datum['location']) })
            );
        }
    }).post({ requestType: "getStages" });
}