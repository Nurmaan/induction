var stageData = [];
var stageLength = 10;
var currentStage = -1;
var lastCheckedTime = 0;

$(document).ready(function () {
    var userToken = localStorage.getItem("userToken");
    lastCheckedTime = localStorage.getItem("lastCheckedTime") || 0;
    if (typeof userToken == "undefined" || userToken == null) {
        redirectToLogin();
    } else {
        listenToInput();
        new Http(baseSession, function () {
            getStage();
            checkGame();
        }, function () {
            redirectToLogin();
        }).post({ requestType: "sessionTest", userToken: userToken });
    }
});

function redirectToLogin() {
    setTimeout(function () {
        window.location.href = baseUrl + "setup/";
    }, 1000);
}

function getStage(stage) {
    new Http(baseStageApi, function (data) {
        stageData = data;
        displayStage(data.length - 1);
        document.getElementById("codeInput").value = "";
    }).post({ requestType: "getStage", stage: stage, userToken: localStorage.getItem("userToken") });
}

function displayStage(index) {
    //reset input
    try {
        document.getElementById("codeInput").value = "";
    } catch (e) { };
    currentStage = index;
    if (index >= stageData.length) {
        toastr.warning("Level Locked");
        return;
    }
    var data = stageData[index];
    var count = 0;

    //clear carousel
    document.getElementById("carouselIndicators").innerHTML = "";
    document.getElementById("carouselImages").innerHTML = "";

    //display images
    for (var image of data['images']) {
        document.getElementById("carouselIndicators").appendChild(
            createElement("li", { "data-target": "#playCarousel", "data-slide-to": count, class: (count == 0 ? "active" : "") })
        );

        document.getElementById("carouselImages").appendChild(
            createElement("div", { class: "carousel-item" + (count == 0 ? " active" : "") }, [
                createElement("img", { class: "d-block w-100 imageZoom", src: baseUploadImage + image })
            ])
        )

        count++;
    }

    //remove the code input if stage has already been validated
    if (typeof stageData[index]['location'] != "undefined") {
        $("#validateWrapper").hide();
        $("#location").show();
        document.getElementById("location").innerHTML = pascalCase(stageData[index]['location']);
    } else {
        $("#validateWrapper").show();
        $("#location").hide();
    }

    initImageZoom();

    //display hint
    document.getElementById("hint").innerHTML = data['hint'];

    displayStageButtons(index);
    displayEquations();
}

function listenToInput() {
    var input = document.getElementById("codeInput");

    input.addEventListener("keyup", function (event) {
        if (event.keyCode === 13) {
            validateCode();
        }
    });
}

function displayEquations() {
    var elem = document.getElementById("equationBody");
    elem.innerHTML = "";
    for (var stage of stageData) {
        if (typeof stage['equation'] != "undefined") {
            elem.appendChild(
                createElement("div", { class: "p-2" }, [
                    createElement("div", { class: "text-center w-100" }, [], { innerHTML: stage['equation'] }),
                    createElement("hr")
                ])
            );
        }

    }
}

function displayStageButtons(index) {
    //clear buttons
    document.getElementById("stageButtons").innerHTML = "";
    for (var i = 0; i < stageLength; i++) {
        document.getElementById("stageButtons").appendChild(
            createElement("div", { class: 'button btn-secondary col ' + (i < stageData.length ? "" : " disabled") + (i == index ? " active" : ""), onclick: "displayStage(" + i + ")" }, [], { innerHTML: i + 1 })
        )
    }
}

function validateCode() {
    var res = result(document.getElementById("validateWrapper"));
    if (res) {
        new Http(baseStageApi, function (data) {
            toastr.success("Code Valid. Found " + data + "!");
            stageData[currentStage]['location'] = data;
            document.getElementById("codeInput").value = "";
            getStage();
        }).post({ requestType: "validate", password: res['password'], userToken: localStorage.getItem("userToken"), stageID: stageData[currentStage]['stageID'] })
    } else {
        toastr.warning("Fill in the input Properly");
    }
}

function toggleEquations() {
    $("#equations").toggleClass("active");
}

function checkGame() {
    new Http(baseStageApi, function (data) {
        var timeout = true;
        for (var datum of data) {
            if (datum['state']) {

                lastCheckedTime = Date.now() / 1000;
                localStorage.setItem("lastCheckedTime", lastCheckedTime);
                window.navigator.vibrate([50, 100, 25, 75]);
                switch (datum['type']) {
                    case "stage":
                        toastr.success(datum['user'] + " Just Found The Next Clue 😎!");
                        setTimeout(function () {
                            getStage();
                        }, 3000);
                        break;

                    case "user":
                        for (var user of datum['user']) {
                            toastr.success(user + " Just Joined Your Team 😀!");
                        }
                        break;

                    case "removeUser":
                        for (var user of datum['user']) {
                            toastr.warning(user + " Left The Party 🙄");
                        }
                        break;

                    case "completed":
                        toastr.success(datum['user'] + " Just got the Secret Code 🤩!");
                        timeout = false;
                }
            }
        }

        if (timeout) {
            setTimeout(function () {
                checkGame();
            }, 2500);
        }
    }, function(){
        redirectToLogin();
    }).post({ requestType: "check", userToken: localStorage.getItem("userToken"), time: lastCheckedTime });
}

function confirmCode() {
    var validStage = stageData.filter(x => typeof x.location != "undefined");
    var res = result(document.getElementById("equationFooter"));
    if (validStage.length >= 10 && res) {
        new Http(baseStageApi, function () {
            toastr.success("Code Valid. Game Completed 😎");
        }).post({ requestType: "checkCode", userToken: localStorage.getItem("userToken"), password: res['secretCode'] });
    } else {
        toastr.warning("You need to complete all the stages before you can submit a code");
    }
}