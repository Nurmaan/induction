$(document).ready(function(){
    getCompleted();
});

function getCompleted(){
    new Http(baseStageApi, function(data){
        displayUITable(document.getElementById("table"), data);
        setTimeout(function(){
            getCompleted();
        }, 1000);
    }).post({requestType:"getCompleted"});
}